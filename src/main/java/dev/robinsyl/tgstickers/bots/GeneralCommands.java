package dev.robinsyl.tgstickers.bots;

import dev.robinsyl.tgstickers.services.DatabaseService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardRow;

import java.util.Collections;

import static org.telegram.telegrambots.abilitybots.api.objects.Locality.ALL;
import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class GeneralCommands extends BaseCommands implements AbilityExtension {

    private final DatabaseService db;
    private final Counter startCounter;
    private final Counter helpCounter;
    private final Counter privacyCounter;
    private final Counter aboutCounter;
    private final Counter donateCounter;

    @Autowired
    public GeneralCommands(MeterRegistry meterRegistry, MessageSource messageSource, DatabaseService db) {
        super(meterRegistry, messageSource);
        this.db = db;
        startCounter = createCounter("start");
        helpCounter = createCounter("help");
        privacyCounter = createCounter("privacy");
        aboutCounter = createCounter("about");
        donateCounter = createCounter("donate");
    }

    @SuppressWarnings("unused")
    public Ability start() {
        return Ability.builder()
                .name("start")
                .info("Start the bot")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    db.addOrUpdateUser(ctx.user());
                    if (ctx.arguments().length >= 1) {
                        if (ctx.firstArg().equals("addsticker")) {
                            sendString("bot.welcome.send", ctx);
                        }
                    } else {
                        sendString("bot.welcome", ctx);
                    }
                })
                .post(ctx -> startCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability help() {
        return Ability.builder()
                .name("help")
                .info("How to use the bot")
                .locality(ALL)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> sendString("bot.help", ctx, ctx.bot().getBotUsername()))
                .post(ctx -> helpCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability privacy() {
        return Ability.builder()
                .name("privacy")
                .info("Regarding your personal information")
                .locality(USER)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> sendString("bot.privacy", ctx))
                .post(ctx -> privacyCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability about() {
        return Ability.builder()
                .name("about")
                .info("About this bot")
                .locality(USER)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> sendString("bot.about", ctx))
                .post(ctx -> aboutCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability donate() {
        return Ability.builder()
                .name("donate")
                .info("Like the bot? Support me on Ko-fi.")
                .locality(USER)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> {
                    BaseAbilityBot bot = ctx.bot();
                    bot.getSilent().sendMd(getString("bot.donate.button.pressed", ctx, formatUser(ctx.user())), bot.creatorId());
                    String message = getString("bot.donate.message", ctx);
                    InlineKeyboardButton button = InlineKeyboardButton.builder()
                            .text(getString("bot.donate.button.text", ctx))
                            .url(getString("bot.donate.button.link", ctx))
                            .build();
                    InlineKeyboardRow row = new InlineKeyboardRow(button);
                    SendMessage sm = SendMessage.builder()
                            .text(message)
                            .chatId(String.valueOf(ctx.chatId()))
                            .replyMarkup(new InlineKeyboardMarkup(Collections.singletonList(row)))
                            .build();
                    bot.getSilent().execute(sm);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> donateCounter.increment())
                .build();
    }

    private String formatUser(User user) {
        // [Tom Scott](tg://user?id=314159)
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(user.getFirstName());
        if (user.getLastName() != null) {
            sb.append(' ');
            sb.append(user.getLastName());
        }
        sb.append("](tg://user?id=");
        sb.append(user.getId());
        sb.append(')');
        return sb.toString();
    }
}

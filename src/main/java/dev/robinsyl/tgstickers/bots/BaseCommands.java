package dev.robinsyl.tgstickers.bots;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.context.MessageSource;
import org.telegram.telegrambots.abilitybots.api.objects.MessageContext;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.Locale;
import java.util.Optional;

import static java.util.function.Predicate.not;

public class BaseCommands {

    protected final MeterRegistry meterRegistry;
    protected final MessageSource messageSource;

    protected BaseCommands(MeterRegistry meterRegistry, MessageSource messageSource) {
        this.meterRegistry = meterRegistry;
        this.messageSource = messageSource;
    }

    protected void sendString(String key, MessageContext ctx, Object... args) {
        sendFormatted(getString(key, ctx, args), ctx);
    }

    protected String getString(String key, MessageContext ctx, Object... args) {
        Locale locale = Optional.ofNullable(ctx.user().getLanguageCode())
                .filter(not(String::isBlank))
                .map(Locale::of)
                .orElse(null);
        return getString(key, locale, args);
    }

    protected String getString(String key, Update update, Object... args) {
        User user = null;
        if (update.hasMessage()) {
            user = update.getMessage().getFrom();
        } else if (update.hasCallbackQuery()) {
            user = update.getCallbackQuery().getFrom();
        } else if (update.hasInlineQuery()) {
            user = update.getInlineQuery().getFrom();
        }
        if (user == null || user.getLanguageCode() == null) {
            return getString(key, Locale.getDefault(), args);
        }
        return getString(key, getLocale(user.getLanguageCode()), args);
    }

    protected String getString(String key, Locale locale, Object... args) {
        if (locale == null) {
            return messageSource.getMessage(key, args, Locale.ENGLISH);
        }
        return messageSource.getMessage(key, args, locale);
    }

    private Locale getLocale(String code) {
        String[] parts = code.split("_");
        return switch (parts.length) {
            case 1 -> Locale.of(parts[0]);
            case 2 -> Locale.of(parts[0], parts[1]);
            case 3 -> Locale.of(parts[0], parts[1], parts[2]);
            default -> Locale.getDefault();
        };
    }

    protected void sendFormatted(String message, MessageContext ctx) {
        SendMessage sendMessage = new SendMessage(ctx.chatId().toString(), message);
        sendMessage.setParseMode(ParseMode.MARKDOWN);
        ctx.bot().getSilent().execute(sendMessage);
    }

    protected Counter createCounter(String commandName) {
        return meterRegistry.counter("bot.command.calls", "command", commandName);
    }
}

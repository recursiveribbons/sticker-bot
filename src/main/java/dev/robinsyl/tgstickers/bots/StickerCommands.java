package dev.robinsyl.tgstickers.bots;

import dev.robinsyl.tgstickers.services.DatabaseService;
import dev.robinsyl.tgstickers.services.StateService;
import dev.robinsyl.tgstickers.types.State;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.objects.Reply;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.methods.stickers.GetStickerSet;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultsButton;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.cached.InlineQueryResultCachedSticker;
import org.telegram.telegrambots.meta.api.objects.stickers.Sticker;
import org.telegram.telegrambots.meta.api.objects.stickers.StickerSet;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import static org.telegram.telegrambots.abilitybots.api.objects.Flag.*;
import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class StickerCommands extends BaseCommands implements AbilityExtension {
    private static final Predicate<Update> STICKER = u -> u.getMessage().hasSticker();

    private final DatabaseService db;
    private final StateService states;
    private final Counter cancelCounter;
    private final Counter removeCounter;
    private final Counter addPackCounter;
    private final Counter removeTagsCounter;
    private final Counter checkTagsCounter;
    private final Counter queryCounter;

    @Autowired
    public StickerCommands(MeterRegistry meterRegistry, MessageSource messageSource, DatabaseService db, StateService states) {
        super(meterRegistry, messageSource);
        this.db = db;
        this.states = states;
        cancelCounter = createCounter("cancel");
        removeCounter = createCounter("remove");
        addPackCounter = createCounter("add_pack");
        removeTagsCounter = createCounter("remove_tags");
        checkTagsCounter = createCounter("tags");
        queryCounter = createCounter("query");
    }

    @SuppressWarnings("unused")
    public Ability cancel() {
        return Ability.builder()
                .name("cancel")
                .info("Cancel the current operation")
                .locality(USER)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> {
                    states.removeState(ctx.user());
                    sendString("bot.cancel", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> cancelCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability receiveTags() {
        return Ability.builder()
                .name("default")
                .flag(MESSAGE, TEXT)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    State state = states.getState(ctx.user()).orElse(State.NONE);
                    if (state == State.SENT_STICKER) {
                        Sticker sticker = (Sticker) states.getData(ctx.user()).orElseThrow(IllegalStateException::new);
                        states.removeState(ctx.user());
                        String[] tags = ctx.arguments();
                        db.addTags(ctx.user().getId(), sticker, tags);
                        sendString("bot.sticker.saved", ctx);
                    } else if (state == State.SENT_PACK) {
                        String packName = (String) states.getData(ctx.user()).orElseThrow(IllegalStateException::new);
                        states.removeState(ctx.user());
                        Optional<StickerSet> opt = ctx.bot().getSilent().execute(new GetStickerSet(packName));
                        if (opt.isEmpty()) {
                            sendString("bot.sticker.pack.failed", ctx);
                            return;
                        }
                        String[] tags = ctx.arguments();
                        List<Sticker> stickers = opt.get().getStickers();
                        stickers.forEach(db::addOrUpdateSticker);
                        db.addTags(ctx.user().getId(), stickers, tags);
                        sendString("bot.sticker.pack.saved", ctx);
                    } else if (state == State.REMOVE_TAGS_SENT_STICKER) {
                        String stickerId = (String) states.getData(ctx.user()).orElseThrow(IllegalStateException::new);
                        states.removeState(ctx.user());
                        db.removeTags(ctx.user().getId(), stickerId, ctx.arguments());
                        sendString("bot.sticker.tags.removed", ctx);
                    } else {
                        states.setState(ctx.user(), State.SENT_TAGS, ctx.update().getMessage().getText());
                        sendString("bot.sticker.tags.received", ctx);
                    }
                    db.addOrUpdateUser(ctx.user());
                })
                .build();
    }

    @SuppressWarnings("unused")
    public Ability remove() {
        return Ability.builder()
                .name("remove")
                .info("Remove a sticker")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    states.setState(ctx.user(), State.DELETE_STICKER);
                    sendString("bot.sticker.remove.send", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> removeCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability addPack() {
        return Ability.builder()
                .name("addpack")
                .info("Add and tag a sticker pack")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    states.setState(ctx.user(), State.ADD_PACK);
                    sendString("bot.sticker.pack.send.pack", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> addPackCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability removeTags() {
        return Ability.builder()
                .name("removetags")
                .info("Remove tags from a sticker")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    states.setState(ctx.user(), State.REMOVE_TAGS);
                    sendString("bot.sticker.tags.remove.send.sticker", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> removeTagsCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability checkTags() {
        return Ability.builder()
                .name("tags")
                .info("Check sticker tags")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    states.setState(ctx.user(), State.CHECK_TAG);
                    sendString("bot.sticker.tags.check", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> checkTagsCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Reply receiveSticker() {
        BiConsumer<BaseAbilityBot, Update> action = (bot, u) -> {
            State state = states.getState(getUser(u)).orElse(State.NONE);
            Sticker sticker = u.getMessage().getSticker();
            db.addOrUpdateSticker(sticker);
            switch (state) {
                case DELETE_STICKER -> {
                    states.removeState(getUser(u));
                    db.removeSticker(getUserId(u), getStickerId(u));
                    sendString("bot.sticker.removed", u, bot);
                }
                case REMOVE_TAGS -> {
                    states.setState(getUser(u), State.REMOVE_TAGS_SENT_STICKER, getStickerId(u));
                    sendString("bot.sticker.tags.remove.send.tags", u, bot);
                }
                case ADD_PACK -> {
                    states.removeState(getUser(u));
                    GetStickerSet req = new GetStickerSet(sticker.getSetName());
                    Optional<StickerSet> result = bot.getSilent().execute(req);
                    if (result.isPresent()) {
                        StickerSet stickerSet = result.get();
                        states.setState(getUser(u), State.SENT_PACK, stickerSet.getName());
                        sendString("bot.sticker.pack.send.tags", u, bot);
                    } else {
                        sendString("bot.sticker.pack.failed", u, bot);
                    }
                }
                case CHECK_TAG -> {
                    states.removeState(getUser(u));
                    Set<String> tags = db.getTags(getUserId(u), getStickerId(u));
                    if (tags.isEmpty()) {
                        sendString("bot.sticker.tags.empty", u, bot);
                    } else {
                        sendString("bot.sticker.tags", u, bot, String.join(" ", tags));
                    }
                }
                case SENT_TAGS -> {
                    String tagString = (String) states.getData(getUser(u)).orElseThrow(IllegalStateException::new);
                    String[] t = tagString.split(" ");
                    states.removeState(getUser(u));
                    db.addTags(getUserId(u), getSticker(u), t);
                    sendString("bot.sticker.saved", u, bot);
                }
                default -> {
                    states.setState(getUser(u), State.SENT_STICKER, getSticker(u));
                    db.getTaggedSticker(getUserId(u), getStickerId(u))
                            .ifPresent(s -> sendString("bot.sticker.tags", u, bot, String.join(" ", s.tags())));
                    sendString("bot.sticker.tags.send.tags", u, bot);
                }
            }
            db.addOrUpdateUser(getUser(u));
        };
        return Reply.of(action, MESSAGE, STICKER);
    }

    @SuppressWarnings("unused")
    public Reply processInlineQuery() {
        BiConsumer<BaseAbilityBot, Update> action = (bot, u) -> {
            queryCounter.increment();
            InlineQuery query = u.getInlineQuery();
            String[] tags = query.getQuery().split(" ");
            int offset = getOffset(query);
            List<InlineQueryResult> results = db.getStickerFileIds(query.getFrom().getId(), offset, tags)
                    .stream()
                    .<InlineQueryResult>map(id -> new InlineQueryResultCachedSticker(id.substring(0, Math.min(id.length(), 64)), id))
                    .toList();
            AnswerInlineQuery answer = createAnswer(u, results);
            bot.getSilent().execute(answer);
        };
        return Reply.of(action, INLINE_QUERY);
    }

    private AnswerInlineQuery createAnswer(Update update, List<InlineQueryResult> results) {
        InlineQuery query = update.getInlineQuery();
        InlineQueryResultsButton button = InlineQueryResultsButton.builder()
                .text(getString("bot.sticker.add", update))
                .startParameter("addsticker")
                .build();
        AnswerInlineQuery answer = AnswerInlineQuery.builder()
                .inlineQueryId(query.getId())
                .results(results)
                .isPersonal(true)
                .button(button)
                .cacheTime(10) // seconds
                .build();
        if (results.size() > 50) {
            String newOffset = String.valueOf(getOffset(query) + results.size());
            answer.setNextOffset(newOffset);
        }
        return answer;
    }

    private int getOffset(InlineQuery query) {
        try {
            return Integer.parseInt(query.getOffset());
        } catch (NumberFormatException | NullPointerException e) {
            return 0;
        }
    }

    private void sendString(String key, Update update, BaseAbilityBot bot, Object... args) {
        bot.getSilent().sendMd(getString(key, update, args), update.getMessage().getChatId());
    }

    private Sticker getSticker(Update update) {
        return update.getMessage().getSticker();
    }

    private String getStickerId(Update update) {
        return update.getMessage().getSticker().getFileUniqueId();
    }

    private User getUser(Update update) {
        return update.getMessage().getFrom();
    }

    private long getUserId(Update update) {
        return update.getMessage().getFrom().getId();
    }
}

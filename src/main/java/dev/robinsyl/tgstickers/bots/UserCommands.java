package dev.robinsyl.tgstickers.bots;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.robinsyl.tgstickers.services.DatabaseService;
import dev.robinsyl.tgstickers.services.StateService;
import dev.robinsyl.tgstickers.types.TaggedSticker;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.objects.Reply;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.message.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardRow;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

import static org.telegram.telegrambots.abilitybots.api.objects.Flag.CALLBACK_QUERY;
import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class UserCommands extends BaseCommands implements AbilityExtension {
    private static final Logger logger = LoggerFactory.getLogger(UserCommands.class);

    private final DatabaseService db;
    private final StateService states;
    private final Counter deleteCounter;
    private final Counter exportCounter;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public UserCommands(MeterRegistry meterRegistry, MessageSource messageSource, DatabaseService db, StateService states) {
        super(meterRegistry, messageSource);
        this.db = db;
        this.states = states;
        deleteCounter = createCounter("delete");
        exportCounter = createCounter("export");
    }

    @SuppressWarnings("unused")
    public Ability delete() {
        return Ability.builder()
                .name("delete")
                .info("Delete all user information")
                .locality(USER)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> {
                    InlineKeyboardRow row = new InlineKeyboardRow();
                    row.add(InlineKeyboardButton.builder().text("Yes").callbackData("delete yes").build());
                    row.add(InlineKeyboardButton.builder().text("No").callbackData("delete no").build());
                    SendMessage send = SendMessage.builder()
                            .chatId(String.valueOf(ctx.chatId()))
                            .text(getString("bot.stop.confirm", ctx))
                            .parseMode(ParseMode.MARKDOWN)
                            .replyMarkup(new InlineKeyboardMarkup(Collections.singletonList(row)))
                            .build();
                    ctx.bot().getSilent().execute(send);
                })
                .post(ctx -> deleteCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Reply processCallbackQuery() {
        BiConsumer<BaseAbilityBot, Update> action = (bot, u) -> {
            String response = getString("bot.query.processed", u);
            CallbackQuery query = u.getCallbackQuery();
            User user = query.getFrom();
            String data = query.getData();
            if (data.equals("delete yes")) {
                states.removeState(user);
                db.removeUser(user);
                EditMessageText edit1 = EditMessageText.builder()
                        .inlineMessageId(query.getInlineMessageId())
                        .text(getString("bot.user.delete.confirmed", u))
                        .build();
                bot.getSilent().execute(edit1);
                response = getString("bot.user.delete.confirmed", u);
            } else if (data.equals("delete no")) {
                EditMessageText edit2 = EditMessageText.builder()
                        .inlineMessageId(query.getInlineMessageId())
                        .text(getString("bot.user.delete.cancelled", u))
                        .build();
                bot.getSilent().execute(edit2);
                response = getString("bot.user.delete.cancelled", u);
            }
            AnswerCallbackQuery answer = AnswerCallbackQuery.builder()
                    .callbackQueryId(query.getId())
                    .text(response)
                    .build();
            bot.getSilent().execute(answer);
        };
        return Reply.of(action, CALLBACK_QUERY);
    }

    @SuppressWarnings("unused")
    public Ability export() {
        return Ability.builder()
                .name("export")
                .info("Export tags to JSON")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    db.addOrUpdateUser(ctx.user());
                    Set<TaggedSticker> stickers = db.export(ctx.user());
                    try {
                        File jsonFile = Files.createTempFile("export", ".json").toFile();
                        objectMapper.writeValue(jsonFile, stickers);
                        InputFile inputFile = new InputFile(jsonFile, "export.json");
                        SendDocument sendDocument = SendDocument.builder()
                                .chatId(ctx.chatId())
                                .document(inputFile)
                                .build();
                        CompletableFuture<Message> future = ctx.bot().getTelegramClient().executeAsync(sendDocument);
                        future.whenComplete(((m, throwable) -> {
                            if (throwable != null) {
                                sendString("bot.export.failed", ctx);
                                logger.error("Unable to send export file", throwable);
                            }
                            try {
                                Files.delete(jsonFile.toPath());
                            } catch (IOException e) {
                                logger.error("Unable to delete temporary export file", e);
                            }
                        }));
                    } catch (Exception e) {
                        sendString("bot.export.failed", ctx);
                        logger.error("Unable to create export file", e);
                    }
                })
                .post(ctx -> exportCounter.increment())
                .build();
    }
}

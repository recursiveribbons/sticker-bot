package dev.robinsyl.tgstickers.types;

import org.telegram.telegrambots.meta.api.objects.stickers.Sticker;

public enum State {
    NONE(false, Object.class),
    SENT_STICKER(true, Sticker.class), DELETE_STICKER(false, Object.class),
    ADD_PACK(false, Object.class), SENT_PACK(true, String.class),
    CHECK_TAG(false, Object.class), SENT_TAGS(true, String.class),
    REMOVE_TAGS(false, Object.class), REMOVE_TAGS_SENT_STICKER(true, String.class);

    private final boolean hasData;
    private final Class<?> dataType;

    State(boolean hasData, Class<?> dataType) {
        this.hasData = hasData;
        this.dataType = dataType;
    }

    public boolean hasData() {
        return hasData;
    }

    public Class<?> getDataType() {
        return dataType;
    }
}

package dev.robinsyl.tgstickers.types;

import java.util.List;

public record TaggedSticker(String id, String fileId, List<String> tags, String set, boolean isAnimated) {
}

package dev.robinsyl.tgstickers.services;

import dev.robinsyl.tgstickers.types.TaggedSticker;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.stickers.Sticker;

import java.io.Closeable;
import java.sql.*;
import java.util.*;

@Service
public class DatabaseService implements Closeable {

    private final Logger logger = LoggerFactory.getLogger(DatabaseService.class);
    private final Connection con;

    public DatabaseService(@Value("jdbc:postgresql://${stickers.db.url}:${stickers.db.port}/${stickers.db.name}") String url, @Value("${stickers.db.user}") String user, @Value("${stickers.db.password}") String password, MeterRegistry meterRegistry) {
        int tries = 1;
        int timeout = 500;
        Connection connection;
        logger.info("Using connection string {}", url);
        while (true) {
            try {
                connection = DriverManager.getConnection(url, user, password);
                break;
            } catch (SQLException e) {
                if (tries++ > 10) {
                    throw new BeanInitializationException("Could not get connection, giving up.", e);
                }
                logger.warn("Could not get connection, try {}", tries);
            }
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                logger.error("Thread interrupted", e);
                Thread.currentThread().interrupt();
            }
            timeout *= 2;
        }
        this.con = connection;
        logger.info("Connected to database");
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate("create table if not exists users(id bigint primary key, first_name varchar(32) not null, last_name varchar(32), username varchar(32), language_code varchar(16));");
            stmt.executeUpdate("create table if not exists stickers(file_unique_id varchar(32) primary key, file_id varchar(128) not null, set_name varchar(32), animated boolean)");
            stmt.executeUpdate("create table if not exists tags(user_id bigint constraint tags_users_id_fk references users on delete cascade, sticker_id varchar(32) constraint tags_stickers_id_fk references stickers, tag varchar(16) not null, constraint tags_pk primary key (user_id, sticker_id, tag));");
            logger.info("Database has been initialised.");
        } catch (SQLException e) {
            throw new BeanInitializationException("Could not initialise database", e);
        }
        meterRegistry.gauge("bot.users", this, DatabaseService::getUserCount);
        meterRegistry.gauge("bot.stickers", this, DatabaseService::getStickerCount);
        meterRegistry.gauge("bot.tags", this, DatabaseService::getTagCount);
    }

    @PreDestroy
    public void close() {
        try {
            con.close();
        } catch (SQLException e) {
            logger.info("Could not close connection", e);
        }
    }

    // Sticker functionality

    /**
     * This method returns the file ID of stickers tagged by a user that matches the given tags, limited to 50 stickers.
     * The tags are matched by prefix, so "foo" will match a sticker tagged with "foobar". Tag matching is case-insensitive.
     * Results are sorted by the number of matched tags.
     * An empty set is returned if no tags are given.
     *
     * @param userId The ID of the user
     * @param offset Offset
     * @param tags   The tags to query, should not be empty
     * @return A {@link List<String>} of file IDs of the stickers tagged by the user. This set is empty if there are no results found, or if there is a database error.
     */
    public List<String> getStickerFileIds(long userId, int offset, String... tags) {
        if (tags.length == 0) {
            return Collections.emptyList();
        }
        List<String> results = new LinkedList<>();

        // Build query

        // WHERE (tag ILIKE ? OR tag ILIKE ? ...)
        String or = " OR tag ILIKE ?".repeat(tags.length - 1);

        try (PreparedStatement stmt = con.prepareStatement("SELECT file_id FROM (SELECT sticker_id, count(*) as weight FROM tags t WHERE user_id=? AND (tag ILIKE ?" + or + ") GROUP BY sticker_id ORDER BY weight DESC LIMIT 50 OFFSET ?) t INNER JOIN stickers s ON t.sticker_id = s.file_unique_id;")) {
            int i = 1;
            stmt.setLong(i++, userId); // WHERE user_id = ?
            for (String tag : tags) {
                // WHERE tag ILIKE 'syl%'
                stmt.setString(i++, tag.toLowerCase() + '%');
            }
            stmt.setInt(i, offset); // OFFSET ?

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        results.add(rs.getString("file_id"));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in getStickerFileIds!", e);
            return Collections.emptyList();
        }
        return results;
    }

    /**
     * This method returns a {@link TaggedSticker} object of a sticker tagged by the user.
     *
     * @param userId    The ID of the user
     * @param stickerId The ID of the sticker
     * @return An {@link Optional<TaggedSticker>} containing the sticker. This optional is empty if there is no result found, or if there is a database error.
     */
    public Optional<TaggedSticker> getTaggedSticker(long userId, String stickerId) {
        TaggedSticker sticker;

        try (PreparedStatement stmt = con.prepareStatement("SELECT s.file_unique_id, t.tag_string, s.file_id, s.animated, s.set_name FROM (SELECT sticker_id, array_agg(tag) AS tag_array FROM tags WHERE user_id=? AND sticker_id=? GROUP BY sticker_id) t INNER JOIN stickers s ON s.file_unique_id = t.sticker_id;")) {
            stmt.setLong(1, userId); // WHERE user_id = ?
            stmt.setString(2, stickerId); // WHERE sticker_id = ?

            // Execute and access results

            boolean result = stmt.execute();
            if (!result) {
                return Optional.empty();
            }
            try (ResultSet rs = stmt.getResultSet()) {
                if (!rs.next()) {
                    return Optional.empty();
                }
                List<String> tags = Arrays.asList((String[]) rs.getArray("tag_array").getArray());
                sticker = new TaggedSticker(rs.getString("sticker_id"), rs.getString("file_id"), tags, rs.getString("set_name"), rs.getBoolean("animated"));
            }
        } catch (SQLException e) {
            logger.error("Database error in getSticker!", e);
            return Optional.empty();
        }
        return Optional.of(sticker);
    }

    /**
     * This method adds any number of tags to a given sticker for a particular user.
     * If no tags are given, this method does nothing.
     * This method calls {@link #addOrUpdateSticker(Sticker)} to ensure that the sticker exists in the database.
     *
     * @param userId  The ID of the user
     * @param sticker The {@link Sticker} to be tagged
     * @param tags    The tags to add, should not be empty
     * @see #addTags(long, List, String...)
     */
    public void addTags(long userId, Sticker sticker, String... tags) {
        if (tags.length == 0) {
            return;
        }
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO tags(user_id, sticker_id, tag) VALUES(?,?,?);")) {
            addOrUpdateSticker(sticker);
            for (String tag : tags) {
                try {
                    stmt.setLong(1, userId);
                    stmt.setString(2, sticker.getFileUniqueId());
                    stmt.setString(3, tag);
                    stmt.executeUpdate();
                } catch (SQLException e) {
                    logger.info("Error in addTags, possibly an attempt to add a duplicate tag", e);
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in addTags!", e);
        }
    }

    /**
     * This method adds any number of tags to any number of stickers for a particular user.
     * If no stickers or tags are given, this method does nothing.
     * This method calls {@link #addTags(long, Sticker, String...)} for all stickers in the list.
     *
     * @param userId   The ID of the user
     * @param stickers A {@link List<Sticker>} to be tagged
     * @param tags     The tags to add, should not be empty
     * @see #addTags(long, Sticker, String...)
     */
    public void addTags(long userId, List<Sticker> stickers, String... tags) {
        if (tags.length == 0) {
            return;
        }
        stickers.forEach(sticker -> addTags(userId, sticker, tags));
    }

    /**
     * This method removes any number of tags from the given sticker for the given user.
     *
     * @param userId    The ID of the user
     * @param stickerId The ID of the sticker to remove
     * @param tags      The tags to remove from the sticker
     */
    public void removeTags(long userId, String stickerId, String... tags) {
        if (tags.length == 0) {
            return;
        }
        try (PreparedStatement stmt = con.prepareStatement("DELETE FROM tags WHERE user_id=? AND sticker_id=? AND tag=?;")) {
            for (String tag : tags) {
                stmt.setLong(1, userId);
                stmt.setString(2, stickerId);
                stmt.setString(3, tag);
                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error("Database error in removeTags!", e);
        }
    }

    /**
     * This method returns the tags assigned to the given sticker by the given user.
     *
     * @param userId    The ID of the user
     * @param stickerId The ID of the sticker
     * @return The {@link Set<String>} of tags that are assigned by the user to the sticker
     */
    public Set<String> getTags(long userId, String stickerId) {
        Set<String> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT tag FROM tags WHERE user_id=? AND sticker_id=?")) {
            stmt.setLong(1, userId);
            stmt.setString(2, stickerId);

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        results.add(rs.getString("tag"));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in getTags!", e);
            return Collections.emptySet();
        }
        return results;
    }

    /**
     * This method removes all tags assigned to the given sticker by the given user.
     *
     * @param userId    The ID of the user
     * @param stickerId The ID of the sticker to remove
     */
    public void removeSticker(long userId, String stickerId) {
        try (PreparedStatement stmt = con.prepareStatement("DELETE FROM tags WHERE user_id=? AND sticker_id=?;")) {
            stmt.setLong(1, userId);
            stmt.setString(2, stickerId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in removeSticker!", e);
        }
    }

    /**
     * This method gives the top 50 tags of a user.
     *
     * @param userId The ID of the user
     * @return A {@link Set<String>} of tags
     */
    public Set<String> listTags(long userId) {
        Set<String> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT tag, count(*) AS tag_count FROM tags WHERE user_id=? GROUP BY tag ORDER BY tag_count DESC LIMIT 50;")) {
            stmt.setLong(1, userId);

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        results.add(rs.getString("tag"));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in listTags!", e);
            return Collections.emptySet();
        }
        return results;
    }

    // User functionality

    /**
     * This method adds a user to the database. If the user already exists, the user data will be updated.
     *
     * @param user The user to add or update
     */
    public void addOrUpdateUser(User user) {
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO users(id, first_name, last_name, username, language_code) VALUES(?,?,?,?,?) ON CONFLICT (id) DO UPDATE SET first_name=?, last_name=?, username=?, language_code=?;")) {
            // INSERT
            stmt.setLong(1, user.getId());
            stmt.setString(2, user.getFirstName());
            stmt.setString(3, user.getLastName());
            stmt.setString(4, user.getUserName());
            stmt.setString(5, user.getLanguageCode());
            // ON CONFLICT UPDATE
            stmt.setString(6, user.getFirstName());
            stmt.setString(7, user.getLastName());
            stmt.setString(8, user.getUserName());
            stmt.setString(9, user.getLanguageCode());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in addOrUpdateUser!", e);
        }
    }

    /**
     * This method adds a sticker to the database. If the sticker already exists, it will update the file id and sticker set fields.
     *
     * @param sticker The sticker to add or update
     */
    public void addOrUpdateSticker(Sticker sticker) {
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO stickers(file_unique_id, file_id, set_name, animated) VALUES(?,?,?,?) ON CONFLICT (file_unique_id) DO UPDATE SET file_id=?, set_name=?, animated=?;")) {
            // INSERT
            stmt.setString(1, sticker.getFileUniqueId());
            stmt.setString(2, sticker.getFileId());
            stmt.setString(3, sticker.getSetName());
            stmt.setBoolean(4, sticker.getIsAnimated());
            // ON CONFLICT UPDATE
            stmt.setString(5, sticker.getFileId());
            stmt.setString(6, sticker.getSetName());
            stmt.setBoolean(7, sticker.getIsAnimated());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in addOrUpdateSticker!", e);
        }
    }

    /**
     * This method exports all tag data for a user.
     *
     * @param user The user to export data for
     */
    public Set<TaggedSticker> export(User user) {
        Set<TaggedSticker> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT s.file_unique_id, s.file_id, s.animated, s.set_name, t.tag_array FROM (SELECT sticker_id, array_agg(tag) AS tag_array FROM tags WHERE user_id=? GROUP BY sticker_id) t INNER JOIN stickers s ON t.sticker_id = s.file_unique_id")) {
            stmt.setLong(1, user.getId());

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        List<String> tags = Arrays.asList((String[]) rs.getArray("tag_array").getArray());
                        results.add(new TaggedSticker(rs.getString("file_unique_id"), rs.getString("file_id"), tags, rs.getString("set_name"), rs.getBoolean("animated")));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in export!", e);
            return Collections.emptySet();
        }

        return results;
    }

    // Stats

    private int getUserCount() {
        int result = 0;
        try (Statement stmt = con.createStatement()) {
            stmt.execute("SELECT count(*) AS num FROM users;");
            try (ResultSet rs = stmt.getResultSet()) {
                if (rs.next()) {
                    result = rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    private int getStickerCount() {
        int result = 0;
        try (Statement stmt = con.createStatement()) {
            stmt.execute("SELECT count(*) AS num FROM stickers;");
            try (ResultSet rs = stmt.getResultSet()) {
                if (rs.next()) {
                    result = rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    private int getTagCount() {
        int result = 0;
        try (Statement stmt = con.createStatement()) {
            stmt.execute("SELECT count(*) AS num FROM tags;");
            try (ResultSet rs = stmt.getResultSet()) {
                if (rs.next()) {
                    result = rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    /**
     * Deletes all user data, including stickers.
     *
     * @param user User whose data to delete
     */
    public void removeUser(User user) {
        try (PreparedStatement stmt = con.prepareStatement("DELETE FROM users WHERE id=?;")) {
            stmt.setLong(1, user.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in removeUser", e);
        }
    }

}
